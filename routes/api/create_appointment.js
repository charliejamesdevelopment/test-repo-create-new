var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');

router.post('/create_appointment',function(req,res){
    if(req.body
      && req.body.parent_id
      && req.body.student_id
      && req.body.teacher_id
      && req.body.date
      && req.body.time
      && req.body.details
      && req.body.type) {
      utils.getGroupByUUID(req.session.uuid, function(group) {
        if(group == 1) {
          console.log('invalid session uuid?= '+req.session.uuid);
          res.send({"response" : 1, "message" : "Invalid uuid!"});
        } else {
          var parent = req.body.parent_id;
          if(group.admin == true || req.session.uuid == parent) {
            var student = req.body.student_id;
            var teacher = req.body.teacher_id;
            var date = req.body.date;
            var time = req.body.time;
            var additional_details = req.body.details;
            var type = req.body.type;
            utils.createAppointment(parent, student, teacher, time, date, additional_details, type, function(response) {
              if(response == 0) {
                res.send({"response" : "0"})
              } else {
                res.send({"response" : "1", "message" : "Something went wrong man!"})
              }
            });
          } else {
            res.send({"response" : "1", "message" : "You can only create an appointment for yourself."})
          }
        }
      });
    } else {
      res.send({"response" : "1", "message" : "Please fill in all fields."})
    }
});

module.exports = router;
